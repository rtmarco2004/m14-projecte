# Scripts 2023-2024

## @edt ASIX-M14

Llistat d'exercicis de M14 Projecte-Scripts

Per baixar/clonar el repositori
```
git clone https://gitlab.com/rtmarco2004/m14-projecte.git
```

Per pujar els apunts:
```
git add . ; git commit -m "act. M14-Projecte" ; git push
``` 
