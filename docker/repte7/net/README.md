# REPTE 7 (net)


## PAS 1:

### CREAR EL DOCKERFILE:

```
FROM debian
LABEL author="marco3008"
LABEL description="repte7 net"
RUN apt-get update && apt-get -y install xinetd iproute2 iputils-ping nmap procps net-tools vsftpd apache2 tftpd-hpa
COPY daytime-stream chargen-stream echo-stream /etc/xinetd.d/
COPY * /tmp
RUN chmod -x /tmp/startup.sh
WORKDIR /tmp
CMD [ "/usr/sbin/xinetd", "-dontfork","startup.sh" ]
EXPOSE 7 13 19
```
Obrirem els ports 7, 13 i 19 que son: echo, daytime i chargen

## PAS 2:

### DESCARREGAR FITXERS:

Descarreguem els fitxers echo-stream, daytime-stream i chargen-stream.

## PAS 3:

Crearem el nostre fitxer index.html per veure la nostra pagina web a traves del port 80.

index.html:

```
<html>
    <head>
        <title>@marco3008</title>
    </head>
    <body>
        <h1>web 2hisx</h1>
        <h2>prueba pagina web</h2>
        mama soy famoso :D
    </body>
</html>
```

## PAS 4:

Crear el nostre startup.sh

```
#!/bin/bash

cp /tmp/index.html /var/www/html/index.html

/etc/init.d/vsftpd start

/etc/init.d/tftpd-hpa start

apache2ctl -D0

/usr/sbin/xinetd -dontfork
```

## COMANDES PER EXECUTAR:

### DOCKERUN:

```
docker run --rm --name prueba_net  -h  ldap.edt.org  -p 7:7 -p 13:13 -p 19:19 -p 80:80  -it marco3008/repte7:net /bin/bash
```

### COMPROVAR QUE ELS PORTS OBERTS FUNCIONEN:

```
nc localhost 7
```

```
nc localhost 13
```

```
nc localhost 19
```
