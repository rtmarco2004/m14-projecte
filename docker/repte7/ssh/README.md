# REPTE 7 (ssh)

## PAS 1:

CREAR EL DOCKERFILE:

```
FROM debian
LABEL subject="repte 7 - SSH"
LABEL author="rtmarco2004"
RUN apt-get update
RUN apt-get install -y openssh-client openssh-server
COPY useradd.sh /tmp
RUN bash /tmp/useradd.sh
WORKDIR /tmp
CMD /etc/init.d/ssh start -D
EXPOSE 22
```
Deixarem el port 22 obert per poder conectar-se per ssh

## PAS 2:

Creem el fitxer useradd.sh

Dins posarem uns quants usuaris per poder-nos conectar per ssh.

```
#! /bin/bash

for usuari in marco ainara marc ivan pere pou prat maria volldamm
do
    useradd -m $usuari
    echo $usuari:$usuari | chpasswd
done
```

## PAS 3:

Creem la imatge

```
docker build -t marco3008/repte7:ssh .
```

Executem la imatge:

```
docker run --rm --name ssh21 -p 2222:22 -d marco3008/repte7:ssh
```

## PROVES:

Fem l'ordre ssh per intentar entrar dins del container per via usuari.

```
ssh -p 2222 marco@localhost
```

La contra es la mateixa que el nom d'usuari.

