#! /bin/bash

mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#configurar samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

#afegir un user per entrar dins
useradd -m -s /bin/bash marco
echo -e "marco\nmarco" | smbpasswd -a marco

/usr/sbin/smbd && echo "smb OK"
/usr/sbin/nmbd -F && echo "nmb OK"
