# REPTE 7 (samba)

## PAS 1:

### Crear el Dockerfile:

```
FROM debian:latest
LABEL author="@marco3008"
LABEL subject="prova de samba server"
RUN apt-get update
RUN apt-get install -y procps samba vim smbclient
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod + /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```
Necesitarem els paquets samba i smbclient per realitzar l'exercici.


## PAS 2:

### Crear el startup.sh:

```
#! /bin/bash

mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#configurar samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

#afegir un user per entrar dins
useradd -m -s /bin/bash marco
echo -e "marco\nmarco" | smbpasswd -a marco

/usr/sbin/smbd && echo "smb OK"
/usr/sbin/nmbd -F && echo "nmb OK"
```

Creem el el /var/lib/samba/public per ficar la xixa

Ficarem la configuració del nostre fitxer smb.conf dins de /etc/samba

Crearem 1 usuari unix/samba i activarem els serveis i confirmarem que s'ha engegat correctament amb un echo


## PAS 3:

### Crear fitxer smb.conf:

```
[global]
workgroup = MYGROUP
server string = Samba Server Version %v
log file = /var/log/samba/log.%m
max log size = 50
security = user
passdb backend = tdbsam
load printers = yes
cups options = raw
[homes]
comment = Home Directories
browseable = no
writable = yes
; valid users = %S
; valid users = MYDOMAIN\%S
[printers]
comment = All Printers
path = /var/spool/samba
browseable = no
guest ok = no
writable = no
printable = yes
[documentation]
comment = Documentació doc del container
path = /usr/share/doc
public = yes
browseable = yes
writable = no
printable = no
guest ok = yes
[manpages]
comment = Documentació man del container
path = /usr/share/man
public = yes
browseable = yes
writable = no
printable = no
guest ok = yes
[public]
comment = Share de contingut public
path = /var/lib/samba/public
public = yes
browseable = yes
writable = yes
printable = no
guest ok = yes
[privat]
comment = Share d'accés privat
path = /var/lib/samba/privat
public = no
browseable = no
writable = yes
printable = no
guest ok = yes
```
Molta xixa

## PAS 4:

### CREAR la imatge:

```
docker build -t marco3008/repte7_samba .
```

## PAS 5:

### Engegar el container:

```
docker run --rm --name samba -p 139:139 -p 445:445 -it marco3008/repte7_samba
```

Els ports 139 i 445 son ports tcp

Dins del container apliquem l'ordre bash startup

```
bash startup.sh
```

Desde una altra consola fem un docker exec:

```
docker exec -it samba /bin/bash
```

## COMPROVACIO:

### Conectar-se dins de samba:

Tenim 2 maneras:

```
smbclient //localhost/public
```

```
smbclient //172.17.0.2/public
```

La contra es el nom de l'unic usuari que hem creat.
