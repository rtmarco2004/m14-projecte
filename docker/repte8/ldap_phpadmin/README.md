# REPTE 8 

## Servidor ldap + phpldapadmin

## PAS 1:

### Crear fitxer .yml

Dins d'aquest fitxer afegim 2 containers per engegar-los a la vegada.

```
vim docker-compose.yml
```

```
version: "3.1"
services:
  ldap:
    image: marco3008/ldap23:editat
    container_name: ldap
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: marco3008/phpadmin
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
```
## PAS 2:

### Executar fitxer

Executarem el nostre fitxer amb la comanda docker-compose

```
docker-compose -f docker-compose.yml up -d
```

## PAS 3:

### Comprovar que els ports estan oberts

```
docker-compose -f docker-compose.yml ps
```

Al realitzar l'ordre es te que mostrar el port 80 i 389 estan oberts.

## COMPROVACIO:

Per comprobar que el phpldapadmin funciona escrivim dins del navegador:

```
localhost/phpldapadmin/
```
Ens portara a la seva web on farem login com anonimous si volem o podem entrar amb usuari cn=Manager,dc=edt,dc=org i passwd secret
