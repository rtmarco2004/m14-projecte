# REPTE 8 

## Servidor Web + portainer

## PAS 1:

### Crear fitxer .yml

Dins d'aquest fitxer afegim 2 containers per engegar-los a la vegada.

```
vim docker-compose.yml
```

```
version: '3.1'

services:

  db:
    image: marco3008/repte3
    restart: always
    environment:
      POSTGRES_PASSWORD: passwd
      POSTGRES_DB: training
    networks:
      - 2hisx
  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
    networks:
      - 2hisx
networks:
  2hisx:
```
## PAS 2:

### Executar fitxer

Executarem el nostre fitxer amb la comanda docker-compose

```
docker-compose -f docker-compose.yml up -d
```

## PAS 3:

### Comprovar que els ports estan oberts

```
docker-compose -f docker-compose.yml ps
```

Al realitzar l'ordre es te que mostrar el port 8080 esta obert.

## COMPROVACIO:

Per comprobar que el servidor web funciona, en el navegador ficarem

```
localhost:8080
```

Ens portara a un login on tenim que escollir les seguents opcions:

1. System: PostgreSQL
2. Server: db
3. Username: postgres
4. Password: passwd
5. Database: training
