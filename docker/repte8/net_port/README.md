# REPTE 8 

## Servidor net + portainer

## PAS 1:

### Crear fitxer .yml

Dins d'aquest fitxer afegim 2 containers per engegar-los a la vegada.

```
vim docker-compose.yml
```

```
version: "3"
services:
  net:
    image: marco3008/repte7_net
    container_name: net.edt.org
    hostname: net.edt.org
    ports:
      - "7:7"
      - "13:13"
      - "19:19"
    networks:
      - mynetwork
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - mynetwork

networks:
  mynetwork:
```
## PAS 2:

### Executar fitxer

Executarem el nostre fitxer amb la comanda docker-compose

```
docker-compose -f docker-compose.yml up -d
```

## PAS 3:

### Comprovar que els ports estan oberts

```
docker-compose -f docker-compose.yml ps
```

Al realitzar l'ordre es te que mostrar el port 7, 13, 19 i 9000 estan oberts.

## COMPROVACIO:

Per comprobar que el portainer funciona fem

```
localhost:9000
```

Ens portara a la web de portainer
