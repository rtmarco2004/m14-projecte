# REPTE 8 

## Servidor Web + portainer

## PAS 1:

### Crear fitxer .yml

Dins d'aquest fitxer afegim 2 containers per engegar-los a la vegada.

```
vim docker-compose.yml
```

```
version: "3"
services:
  web:
    image: marco3008/repte2
    container_name: web.edt.org
    hostname: web.edt.org
    ports:
      - "80:80"
    networks:
      - mynetwork
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - mynetwork

networks:
  mynetwork:
```
## PAS 2:

### Executar fitxer

Executarem el nostre fitxer amb la comanda docker-compose

```
docker-compose -f docker-compose.yml up -d
```

## PAS 3:

### Comprovar que els ports estan oberts

```
docker-compose -f docker-compose.yml ps
```

Al realitzar l'ordre es te que mostrar el port 80 obert y el port 9000 també obert.

## COMPROVACIO:

Per comprobar que el servidor web funciona, en el navegador ficarem

```
localhost:80
```

Per comprobar que el portainer funciona fem

```
localhost:9000
```

Ens portara a la web de portainer
