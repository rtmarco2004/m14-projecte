# REPTE 9

## Comptador de Visites

## PAS 1:

CREAR EL DOCKERFILE:

Primer tenim que crear el nostre Dockerfile on prepararem el nostre comptador.

```
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["flask", "run"]
```

## PAS 2:

Creem el fitxer app.py que conte el programa que comptara les vegades que han entrar a la pagina

```
import time
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
           return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)
@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hola Marco! I have been seen {} times.\n'.format(count)
```

Creem el fitxer requirements perque s'instal·lin els paquets necessaris.

```
vim requirements.txt
```
```
flask
redis
```

## PAS 3:

Crear el docker-compose:

```
version: '3'
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
  redis:
    image: "redis:alpine"
```

El docker-compose ens permet fer canvis en calent en el nostre fitxer app.py

## PAS 4:

Engegar el docker compose

```
docker compose -f docker-compose.yml up -d
```

### Comprovacio:

Dins del navegador escrivim localhost:5000 i ens portara a la web creada

```
localhost:5000
```
