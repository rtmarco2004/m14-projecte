# REPTE 5

## PAS 1:

### CREAR EL DOCKERFILE:

Primer tenim que crear el nostre Dockerfile on prepararem el nostre servidor ldap.

```
FROM debian:latest
LABEL version="1.0"
LABEL autho="@edt ASIX-M06"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT [ "/bin/bash",  "/opt/docker/startup.sh" ]
EXPOSE 389
```
Dins del Dockerfile cambiarem el nostre tipic CMD per un ENTRYPOINT que llegira la info del nostre startup. 

### PAS 2:

Descarreguem el fitxer slapd.conf per la config de slapd i edt-org.ldif que conte una nova base de dades.

### PAS 3:

creem el fitxer startup.sh

```
#!/bin/bash

arg2=$2
case "$1" in
  "slapd")
    echo "Engegant servidor."
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "initdb")
    echo "Creant Servidor LDAP amb les dades de l'escola."
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "start"|"edtorg"|" ")
    echo "Iniciando el servicio LDAP amb les dades per defecte."
    echo "Creant Servidor LDAP amb les dades de l'escola."
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "slapcat")
    if [ $arg2 == "0" ] || [ $arg2 == "1" ]; then
  	slapcat -n$arg2
    elif [ -z "$arg2" ]; then
	  slapcat
    fi
    ;;

  *)
    echo "Uso: $0"
    exit 1
    ;;
esac
```

### Explicacio startup.sh:

Dins del fitxer startup tenim un case que rep el argument 1 i depenent l'argument passat engara el container d'una manera o altra. Si fem slapcat amb, depenent del segon argument (en cas que tingui) fara un slapcat d'una base de dades o d'una altra.



### Crear volums:

```
docker volume create ldap-dades
```
```
docker volume create ldap-config
```

### Comandes per executar:

initdb:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 initdb
```

slapd:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 slapd
```

start|edtorg|res:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v ldap-dades:/var/lib/ldap -v ldap-config:/etc/ldap/slapd.d -p 389:389 -it repte5 start
```

slapcat:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 slapcat
```
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 slapcat 0
```
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 slapcat 1
```
