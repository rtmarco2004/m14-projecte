# REPTE1 

## DOCKERFILE

Primero creamos el fichero DOCKERFILE donde introduciremos todas las ordenes que queremos que nos haga 

```
FROM debian:latest
LABEL author="marco"
RUN apt-get update
RUN apt-get install -y procps iproute2 tree nmap vim
workdir /tmp/prova
```

## GENERAR IMAGEN

Creamos la imagen 

```
docker build -t a221797mr/repte1 .
``` 

## ENTRAR DENTRO DEL CONTAINER 

```
docker run --rm -h edt.org -it a221797mr/repte1 ps ax
```
