# REPTE 6

## PAS 1:

CREAR EL DOCKERFILE:

Primer tenim que crear el nostre Dockerfile on prepararem el nostre servidor ldap.

```
FROM debian:latest
LABEL version="1.0"
LABEL autho="@edt ASIX-M06"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```
## PAS 2:

Creem el fitxer startup.sh .

```
#!/bin/bash

echo "Configurant el servidor ldap..."

sed -i "s/Manager/$LDAP_ROOTDN/g" slapd.conf
sed -i "s/secret/$LDAP_ROOTPW/g" slapd.conf
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
```
afegim adalt 2 sed per canviar el nom del admin(manager) i la contra(secret) del arxiu.

### PAS 3:

Descarregar el fit slapd.conf

### PAS 4:

Fem el docker build de la nostra imatge.

```
docker build -t repte6 .
```

### PAS 5:

Arranquem el container.

```
docker run --rm --name prueba -h ldap.edt.org -p 389:389 -e LDAP_ROOTDN="marco" -e LDAP_ROOTPW="guapo" -it repte6
```

En una altra terminal:

```
docker exec -it prueba /bin/bash
```

### COMPROVACIO:

Per comprobar que hem el canvi s'ha efectuat podem fer cat slapd.conf i veurem els canvis.
