# REPTE2

## DOCKERFILE

Creamos el fichero Dockerfile donde instalaremos systemctl, apache2 para poder hacer nuestra pagina web.

```
FROM debian:latest
LABEL author="Marco Rizzetto"
LABEL subject="repte2"
RUN apt-get update
RUN apt-get install -y apache2 nmap systemctl
COPY index.html /var/www/html/
WORKDIR /tmp
EXPOSE 80
CMD apachectl -k start -X
```

## Index.html

Creamos un fichero html donde meteremos el mensaje que queremos que muestre nuestra pagina web

```
vim index.html
```

## GENERAR IMAGEN

Generamos nuestra imagen detach

```
docker build -t a221797mr/repte2:detach .
```

## CREAR CONTAINER

Creamos nuestro container 

```
docker run --rm -h edt.org -p 80:80 -it a221797mr/repte2:detach
```

## Resultat final

Para comprobar si todo ha salido bien escribimos en el navegador localhost
