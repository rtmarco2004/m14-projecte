# REPTE3

## PostgreSQL:

## PAS1:

Creem el nostre script .sql per la nostre base de dades (trainingv4.sql)


## PAS2:

###  Dockerfile:

Crearem un dockerfile per descarregar la imatge de postgres i copiarem el nostre fitxer dins del docker-entrypoint-initdb.d

```
FROM postgres:latest
COPY trainingv4.sql /docker-entrypoint-initdb.d/
```

## PAS3:

### Construir la imatge i creacio de volum:

Construirem la nostra imatge amb la seguent ordre:

```
docker build -t marco3008/repte3 .
```

Creem un volum per guardar les dades de forma persistent:

```
docker volume create volpostgres
```


## Execucio:

```
docker run --name postgres -e POSTGRES_PASSWORD='jupiter' -v volpostgres:/var/lib/postgresql/data -d marco3008/repte3
```

Li donem un nom al nostre container, el -e es obligatori per al postgres ja que serveix per donar una password al mateix postgres. Assignarem un volum a la carpeta de postgres.
Podem també posar un -e POSTGRES_DB=nom_base_de_dades per assignar una base de dades per defecte.
